#!/bin/bash

if [ $# -eq 0 ]
  then
    echo "Informe a pasta com as normas para mapeamento"
  else
	sudo docker rm lexml-parser-alpine
    	sudo docker run -it --name lexml-parser-alpine -v $1/rtf:/lexml-parser/rtf -v $1/lexml:/lexml-parser/lexml -v $1/log:/lexml-parser/log -v $1/txt:/lexml-parser/txt -t eneiascs/lexml-parser:alpine /bin/bash 		
fi

