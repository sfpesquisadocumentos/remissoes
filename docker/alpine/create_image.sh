#!/bin/bash

parser_folder="/home/eneiascs/workspace-lexml/lexml-parser-projeto-lei"
linker_folder="/home/eneiascs/lexml/lexml-linker/dist/"

echo "Preparando arquivos para a imagem docker do parser lexml"
echo "Empacotando parser lexml"
mvn -f $parser_folder package
parser_version=$(mvn -f $parser_folder org.apache.maven.plugins:maven-help-plugin:2.1.1:evaluate -Dexpression=project.version | grep -v '\[')
echo "Copiando arquivo jar do parser"
parser_jar=lexml-parser-projeto-lei-$parser_version-onejar.jar
cp $parser_folder/target/$parser_jar parser.jar
echo "Compilando linker lexml"
make -C $linker_folder all
echo "Copiando linker lexml"
cp -R $linker_folder linker-dist
sudo docker build -t eneiascs/lexml-parser:alpine .
