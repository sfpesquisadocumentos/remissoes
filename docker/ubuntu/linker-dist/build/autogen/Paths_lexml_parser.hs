module Paths_lexml_parser (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch

version :: Version
version = Version [0,2] []
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/eneiascs/.cabal/bin"
libdir     = "/home/eneiascs/.cabal/lib/x86_64-linux-ghc-7.10.3/lexml-parser-0.2-49Dktro1dMCE2TgB5gpQ43"
datadir    = "/home/eneiascs/.cabal/share/x86_64-linux-ghc-7.10.3/lexml-parser-0.2"
libexecdir = "/home/eneiascs/.cabal/libexec"
sysconfdir = "/home/eneiascs/.cabal/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "lexml_parser_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "lexml_parser_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "lexml_parser_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "lexml_parser_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "lexml_parser_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
