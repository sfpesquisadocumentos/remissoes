#!/bin/bash
for norma in $1'/'*.rtf
do
	sigla=$(echo $norma | awk -F '/|-' '{print $6} ')
	numero=$(echo $norma |awk -F '/|-' '{print $7}')
	ano=$(echo $norma |awk -F '/|-' '{print $8}'|awk -F '.' '{print $1}')
	pasta=$2'/'$ano'/'
	if [ ! -d $pasta ];then
		mkdir $pasta
	fi
	arquivo=$sigla'-'$ano'-'$numero'.rtf'
	echo 'Copiando arquivo '$norma 'para '$pasta$arquivo
	cp $norma $pasta$arquivo	
done 

