#!/bin/bash
if [ $# -lt 2 ]; 
	then 
		echo "2 arguments expected"
	else
		rtf_folder=$(pwd $2)/$2/rtf
		mkdir -p $rtf_folder
		while read norma;
		do
			cod_publicacao=$(echo $norma |awk -F ";" '{print $5}')
			cod_prop=$(echo $norma |awk -F ";|\r" '{print $6}')
			nom_original=$(echo $norma |awk -F ";" '{print $3}')
			letra=$(echo $norma | grep -o '\-[A-Z]\-' | grep -o '[A-Z]')
			nom_original=$(echo $nom_original | sed 's/-[A-Z]-/-/')
			des_nome=$(echo $norma |awk -F ";" '{print $1}')
			ano=$(echo $nom_original |awk -F "-" '{print $3}')
			numero=$(echo $nom_original |awk -F "-" '{print $2}')
			numero=$(printf "%05d" $numero)
			pasta=$rtf_folder/$ano
			NF=$(echo $nom_original |awk -F "-" '{print NF}')
			sufixo=""
			if [ $NF -ge 6 ];then
				sufixo+="-"$(echo $nom_original |awk -F "-" '{print $6}')
			fi
			if [ $NF -ge 7 ];then
				sufixo+="-"$(echo $nom_original |awk -F "-" '{print $7}')
			fi
			if [ $NF -ge 8 ];then
				sufixo+="-"$(echo $nom_original |awk -F "-" '{print $8}')
			fi
			if [ $NF -ge 9 ];then
				sufixo+="-"$(echo $nom_original |awk -F "-" '{print $9}')
			fi
			nom_arquivo='LEI-'$ano'-'$numero$letra$sufixo'.rtf'
			echo 'NF '$NF
			echo 'sufixo '$sufixo
			echo 'nome '$nom_arquivo 
			echo 'Verificando '$des_nome
  			mkdir -p $pasta
 
		if [ ! -f $pasta'/'$nom_arquivo ]; then                   
			echo 'Baixando '$des_nome
			wget1='https://legis.senado.gov.br/sigen/api/instancia/'$cod_publicacao'/ArquivoTextoPublicado/'$cod_prop'/download -O '$pasta'/'$nom_arquivo
			#wget1='https://legis.senado.gov.br/sigen/api/instancia/'$cod_publicacao'/ArquivoTextoPublicado/'$cod_prop'/download -O '$pasta'/'$nom_original.rtf
			echo $wget1
			wget $wget1
		fi
	done <  <(tail -n +2 $1)
fi
