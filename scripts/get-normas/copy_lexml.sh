#!/bin/bash
for norma in $1'/'*.zip
do
	sigla=$(echo $norma | awk -F '/|-' '{print $5} ')
	numero=$(echo $norma |awk -F '/|-' '{print $6}')
	ano=$(echo $norma |awk -F '/|-' '{print $7}')
	sufixo=$(echo $norma |awk -F '/|-' '{print $8}'|awk -F '.' '{print $1}')
	pasta=$2'/'$ano'/'
	pasta_zip=$(echo $norma |awk -F '.' '{print $1}')
	if [ ! -d $pasta ];then
		mkdir $pasta
	fi
	arquivo=$sigla'-'$ano'-'$numero'.xml'
	echo 'Descompactando para '$pasta_zip
	mkdir $pasta_zip
	unzip -o $norma -d $pasta_zip
	echo 'Copiando arquivo '$pasta_zip'/texto.xml para '$pasta$arquivo
	cp $pasta_zip'/texto.xml' $pasta$arquivo	
done 

